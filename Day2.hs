module Day2 (checksum, locateBox) where

import Data.Foldable (find, foldl')
import Data.List (group, sort)

checksum :: String -> Int
checksum = uncurry (*) . foldl' sum2 (0,0) . map (score . group . sort) . lines
    where score xs = (score' 2 xs, score' 3 xs)
          score' len = maybe 0 (const 1) . find ((== len) . length)
          sum2 (x,y) (x',y') = (x+x',y+y')

distance :: String -> String -> Int
distance = go 0
    where go r [] [] = r
          go r (x:xs) (y:ys)
              | x == y = go r xs ys
              | otherwise = go (r+1) xs ys
          go _ _ _ = undefined

locateBox :: String -> String
locateBox = uncurry (commons "") . go . lines
    where go [] = error "Box not located"
          go (x:xs) = maybe (go xs) ((,) x) $ find ((== 1) . distance x) xs
          commons r [] [] = r
          commons r (x:xs) (y:ys)
              | x == y = commons (r <> [x]) xs ys
              | otherwise = commons r xs ys
          commons _ _ _ = undefined
