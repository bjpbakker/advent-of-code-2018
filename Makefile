GHCFLAGS := -Wall -Wincomplete-uni-patterns -Wincomplete-record-updates -Wmissing-import-lists

advent-of-code: $(wildcard *.hs) | build
	ghc --make $(GHCFLAGS) -odir build -hidir build -o $@ Main.hs

build:
	mkdir $@
