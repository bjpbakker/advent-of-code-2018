module Day3 (overlapping, nonOverlapping) where

import Data.Foldable (find, foldl')
import Data.Set (Set, disjoint, empty, fromList, intersection, size, union)
import Text.Regex (Regex, matchRegex, mkRegex)

type ClaimID = String
type Coord = (Int, Int)
type Size = (Int, Int)

data Claim = Claim ClaimID Coord Size
           deriving Show

positions :: Claim -> Set Coord
positions (Claim _ (left,top) (width, height)) = fromList $
    [(x,y) | x <- [left+1..(left+width)]
           , y <- [top+1..(top+height)]]

claimRegex :: Regex
claimRegex = mkRegex "^#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)$"

parseClaim :: String -> Claim
parseClaim s = readClaim $ matchRegex claimRegex s
    where readClaim (Just [id',l,t,w,h]) = Claim id' (read l, read t) (read w, read h)
          readClaim _ = error $ "Invalid claim format: " ++ s

overlaps :: [Claim] -> (Set Coord, Set Coord)
overlaps = foldl' go (empty, empty) . fmap positions
    where go (dup, seen) ps = (dup `union` (seen `intersection` ps), seen `union` ps)

overlapping :: String -> Int
overlapping = (size . fst) . overlaps . fmap parseClaim . lines

nonOverlapping :: String -> ClaimID
nonOverlapping = claimId . locate . fmap parseClaim . lines
    where locate claims = let dups = fst . overlaps $ claims in
                          find (\x -> positions x `disjoint` dups) claims
          claimId (Just (Claim id' _ _)) = id'
          claimId _ = error "All claims are overlapping"
