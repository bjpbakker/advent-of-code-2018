module Day1 (frequency, calibrate) where

import Data.Foldable (foldl')
import Data.IntSet (empty, insert, member)

op :: String -> Int -> Int
op ('+':s) n = n + read s
op ('-':s) n = n - read s
op s _ = error $ "Invalid operation: " ++ s

frequency :: String -> Int
frequency = foldl' (flip op) 0 . lines

calibrate :: String -> Int
calibrate = go (0, empty) . concat . repeat . lines
    where
      go _ [] = error "No repeating frequency"
      go (freq, seen) (x:xs) | member freq seen = freq
                             | otherwise = go (op x freq, insert freq seen) xs
