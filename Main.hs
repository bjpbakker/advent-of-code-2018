{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
module Main where

import Day1 (frequency, calibrate)
import Day2 (checksum, locateBox)
import Day3 (overlapping, nonOverlapping)

solve :: Show a => String -> a -> IO ()
solve puzzle solution = putStrLn $ puzzle ++ ": " ++ (show solution)

day1 = part1 <> part2
    where part1 = frequency <$> input >>= solve "Frequency"
          part2 = calibrate <$> input >>= solve "Calibrate"
          input = readFile "input/day1.txt"

day2 = part1 <> part2
    where part1 = checksum <$> input >>= solve "Checksum"
          part2 = locateBox <$> input >>= solve "Locate box"
          input = readFile "input/day2.txt"

day3 = part1 <> part2
    where part1 = overlapping <$> input >>= solve "# Overlapping claims"
          part2 = nonOverlapping <$> input >>= solve "Non-overlapping claim"
          input = readFile "input/day3.txt"

main = day1 <> day2 <> day3
