{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  haskell = haskellPackages.ghcWithPackages (s: [ s.regex-compat ]);
in mkShell {
  buildInputs = [ haskell ];
}
